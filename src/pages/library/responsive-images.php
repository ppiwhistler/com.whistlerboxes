<?php
/**
 * Configure responsive images sizes
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 2.6.0
 */

// Add featured image sizes
//
// Sizes are optimized and cropped for landscape aspect ratio

// Add additional image sizes
add_image_size( 'xsmall', 200 );
add_image_size( 'small', 800 );
add_image_size( 'medium', 1100 );
add_image_size( 'large', 1600 );
add_image_size( 'xlarge', 2200 );

// Register the new image sizes for use in the add media modal in wp-admin
function foundationpress_custom_sizes( $sizes ) {
	return array_merge(
		$sizes, array(
			'small'  => __( 'Small' ),
			'medium' => __( 'Medium' ),
			'large'  => __( 'Large' ),
			'xlarge' => __( 'XLarge' ),
		)
	);
}
add_filter( 'image_size_names_choose', 'foundationpress_custom_sizes' );

// Add custom image sizes attribute to enhance responsive image functionality for content images
function foundationpress_adjust_image_sizes_attr( $sizes, $size ) {
  
  $width = $size[0];
    
    if ( $width === 600 ) {
        return '(min-width: 768px) 322px, (min-width: 576px) 255px, calc( (100vw - 30px) / 2)';
    }
        
    if ( $width === 1100 ) {
        return '(min-width: 1100px) 100vw' ;
    }
    
    if ( $width === 1600 ) {
        return '(min-width: 1600px) 100vw' ;
    }
    
    if ( $width === 2200 ) {
        return '(min-width: 2200px) 100vw' ;
    }    
        
   return $sizes;
      
  }

add_filter( 'wp_calculate_image_sizes', 'foundationpress_adjust_image_sizes_attr', 10, 2 );



// ACF responsive images
// Remove inline width and height attributes for post thumbnails
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
	if ( ! strpos( $html, 'attachment-shop_single' ) ) {
		$html = preg_replace( '/^(width|height)=\"\d*\"\s/', '', $html );
	}
	return $html;
}
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );


function awesome_acf_responsive_image($image_id,$image_size,$max_width){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';

	}
}

