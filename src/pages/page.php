<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php include("template-parts/slides/homepage.php"); ?>
<div class="doublespacer">&nbsp;</div>
<div class="grid-container">
	<div class="grid-x grid-padding-x align-center">
		<div class="small-12 medium-8 cell">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php
get_footer();
