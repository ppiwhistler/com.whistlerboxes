<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
	<div class="grid-x align-center">
		<main class="small-12 medium-10 cell">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/fullwidth', 'page' ); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
		</main>
	</div>
<?php get_footer();
