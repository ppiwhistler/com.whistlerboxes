<?php
/*
Template Name: Support
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="small-12 medium-6 cell">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/support', 'page' ); ?>
					<?php comments_template(); ?>
				<?php endwhile; ?>
			</div>
			<div class="small-12 medium-5 medium-offset-1 cell">
			<div class="doublespacer">&nbsp;</div>
			<div class="grid-x">
				<div class="small-12 cell">
					<h2>Our Office</h2>
          <p>7700 Cherry Creek South Drive<br>
            Denver, CO, 80231<br>
            United States</p>
          <p>Call us: | <a href="tel:+1-720-619-2901"> (720) 619-2901</a></p>
				</div>
				<div class="small-12 cell">
					<img width="640" src="https://maps.googleapis.com/maps/api/staticmap?center=7700+Cherry+Street+south+DRive+Denver,+CO,+80231&zoom=13&scale=1&size=640x400&maptype=roadmap&key=AIzaSyCPNIpi0bzLbVrObxTHXEsNU8J6i7_XSCo&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xba9c66%7Clabel:%7C7700+Cherry+Street+south+DRive+Denver,+Co" alt="Google Map of 7700 Cherry Street south Drive Denver, CO, 80231">	
				</div>
			</div>
		</div>
		</div>
	</div>

	<div class="grid-x small-up-1 medium-up-4 large-up-3 align-center">
		<div class="cell"><img src="/wp-content/uploads/2019/01/showroom5.jpg"></div>
		<div class="cell"><img src="/wp-content/uploads/2019/01/showroom6.jpg"></div>
		<div class="cell"><img src="/wp-content/uploads/2019/01/showroom7.jpg"></div>
	</div>
<?php get_footer();