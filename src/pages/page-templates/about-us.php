<?php
/*
Template Name: About Us
*/
get_header(); ?>

<!-- <div class="videowrapper" style="background-image: url('/videos/Stinger-wake-surf-drone.jpg');">
	<div class="show-for-medium-up">
		<video id="wakeShaper" autoplay="autoplay" loop="loop" muted="" type="video/mp4">
			<source src="/video/Stinger-wake-surf-drone.mp4" type="video/mp4" />
			<source src="/video/Stinger-wake-surf-drone.WebM" type="video/WebM" />
		</video>
	</div>
</div> -->
<div class="doublespacer">&nbsp;</div>
<section class="lead">
	<div class="grid-x grid-padding-x align-center align-middle">
		<div class="small-11 medium-offset-1 cell">
			<h1 class="text-center"><?php the_title();?></h1>
		</div>
		<div class="small-12 medium-5 medium-offset-1 cell">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/fullwidth', 'page' ); ?>
				<?php comments_template(); ?>
			<?php endwhile; ?>
    </div>
		<div class="small-12 medium-5 medium-offset-1 cell">
		  <button class="thumbnail" type="button" data-open="video"><img src="/images/video/stinger_launch_poster.jpg" alt="" width="480" /></button>
				<div id="video" class="reveal" data-reveal="" data-reset-on-close="true" data-animation-in="fade-in" data-animation-out="fade-out">
					<video id="homevideo" poster="/images/video/stinger_launch_poster.jpg" preload="none" controls="controls" width="100%" height="100%" controlslist="nodownload" type="video/mp4">  <source src="/images/video/stinger_launch.mp4" type="video/mp4" /><source src="/images/video/stinger_launch.WebM" type="video/WebM" /></video>
			<button class="close-button" type="button" aria-label="Dismiss alert" data-close=""> <span aria-hidden="true">×</span> </button></div>
		</div>
	</div>
</section>
<div class="doublespacer">&nbsp;</div>
<?php get_footer();
