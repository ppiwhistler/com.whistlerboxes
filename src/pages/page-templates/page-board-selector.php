<?php
/*
Template Name: Board Selector
*/
get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/board-selector', 'page' ); ?>
		<?php comments_template(); ?>
    <?php endwhile; ?>
  <?php get_footer();
