<?php
/*
Template Name: Contact
*/
get_header(); ?>

<!-- <div class="videowrapper" style="background-image: url('/videos/Stinger-wake-surf-drone.jpg');">
	<div class="show-for-medium-up">
		<video id="wakeShaper" autoplay="autoplay" loop="loop" muted="" type="video/mp4">
			<source src="/video/Stinger-wake-surf-drone.mp4" type="video/mp4" />
			<source src="/video/Stinger-wake-surf-drone.WebM" type="video/WebM" />
		</video>
	</div>
</div> -->
<div class="doublespacer">&nbsp;</div>
<section class="lead">
	<div class="grid-x grid-padding-x align-center">
		<div class="small-12 medium-10 cell">
			<h1 class="text-center"><?php the_title();?></h1>
		</div>
		<div class="small-12 medium-6 cell">
			<?php the_content(); ?>
    </div>
	</div>
</section>
<div class="doublespacer">&nbsp;</div>
<?php get_footer();
