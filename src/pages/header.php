<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta charset="utf-8" />
	  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	  <?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="shortcut icon" href="http://whistlerboxes/favicon.ico" type="image/x-icon" />
		<meta name="google-site-verification" content="sKFNY7sVvqtKQviBqyBTpzLxeTSlsVyOhe2pqSg8Gls" />

		<script type="text/javascript" src="https://use.typekit.com/elm2dsx.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-254815-24']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
		<meta name="google-site-verification" content="_rxrPYtKGax-TVJ8hZewnNmkW_ZnhABsOi-YyMpFDHs" />
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	</head>
	<body <?php body_class(); ?>>

  <div data-sticky-container>
		<div class="navigation" data-sticky data-options="marginTop:0;" data-sticky-on="small" style="width:100%" data-btm-anchor="content:bottom">
  		<div class="site-title-bar title-bar hide-for-medium" <?php foundationpress_title_bar_responsive_toggle(); ?>>
        <div class="grid-x">
      		<div class="small-2 small-offset-1 cell">
      			<button aria-label="Menu Icon" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
      		</div>
      		<div class="small-8 small-offset-1 cell logo">
      			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Whistler<span>Boxes</span><span>.com</span></a>
      		</div>
      	</div>
      </div>
      <div class="top-bar hide-for-medium" id="<?php foundationpress_title_bar_responsive_toggle(); ?>">
        <div class="top-bar-left">
          <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
        </div>
      </div>

      <div id="menu" class="top-bar show-for-medium" data-animate="fade-in fade-out" data-show-for="medium">
        <div class="top-bar-left">
			    <div class="grid-container grid-x align-middle">
            <div class="medium-2 cell logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Whistler<span>Boxes</span><span>.com</span></a>
            </div>
            <div class="medium-9 cell">
        			<?php foundationpress_top_bar_l(); ?>
            </div>
				    <div class="medium-1 cell">
					    <div class="grid-x align-middle small-up-1">
                <div class="cell">
                  <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
                    <span class="text-right"><?php echo sprintf ( _n( '%d ', '%d  ', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span>
                    <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>
