<?php
/**
	 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

  <?php include("template-parts/slides/homepage.php"); ?>

	<section id="content">

	<div class="grid-container grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2">
			<div class="cell">
				<div class="grid-x grid-padding-x">
					<div class="small-9 cell"><h1>Whistler<strong>Boxes</strong>.com </h1>
					<h2>Eco-friendly moving boxes.</h2></div>
					<div class="small-3 cell"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/box.jpg"></div>
				</div>
				<div class="small-10 cell">
					<p> Our moving boxes are made from heaving duty polyethylene, with attached tops that have steel hinges that interlock securely for stacking strength. Water resistant, durable.</p>
				</div>
			</div>
			<div class="cell">
				<div class="grid-x grid-padding-x">
					<div class="small-9 cell">
						<h1>Order<strong>Online</strong></h1>
						<h2>Serving the Sea To Sky Corridor.</h2>
					</div>
					<div class="small-3 cell">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/truck.jpg">
					</div>
					<div class="small-10 cell">
						<p> Use our convenient online order form to place your order of our Eco-Friendly moving boxes.  Available for delivery in Whistler, Pemberton &amp; Squamish we can help you make your moving day easier!</p>
					</div>
			</div>
		</div>
	</div>

  </section>
  <?php get_footer();
