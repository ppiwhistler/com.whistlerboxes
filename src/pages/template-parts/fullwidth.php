<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="grid-x grid-padding-x align-center">
		<div class="small-11 medium-offset-1 cell">
			<h1 class="text-center"><?php the_title();?></h1>
		</div>
  </div>
<div class="grid-container">
  <div class="grid-x grid-margin-x grid-margin-y align-center small-up-1 medium-up-3">
    
		<?php the_content(); ?>
		<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
	</div>
</div>
<footer>
	<?php
		wp_link_pages(
			array(
				'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
				'after'  => '</p></nav>',
			)
		);
	?>
	<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
</footer>
</article>
