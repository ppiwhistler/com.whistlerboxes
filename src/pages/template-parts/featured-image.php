<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header class="featured-hero" role="banner" data-interchange="[<?php the_post_thumbnail_url( 'journal-thumb' ); ?>, small], [<?php the_post_thumbnail_url( 'journal-m' ); ?>, medium], [<?php the_post_thumbnail_url( 'journal-l' ); ?>, large], [<?php the_post_thumbnail_url( 'journal-xl' ); ?>, xlarge]">
	</header>
<?php endif;
