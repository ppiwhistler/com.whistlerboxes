  <div class="orbit" aria-label="Stinger Foils &amp; Boards Homepage" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
    <div class="orbit-wrapper">
      <div class="orbit-controls"><button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>◀︎</button> <button class="orbit-next"><span class="show-for-sr">Next Slide</span>▶︎</button></div>
      <ul class="orbit-container">
        <li class="is-active orbit-slide">
          <figure class="orbit-figure"><img class="orbit-image" alt="" data-interchange="[/images/hero/Stinger-Foils-header-wake-surfing-s.jpg, small], [/images/hero/Stinger-Foils-header-wake-surfing-m.jpg, medium], [/images/hero/Stinger-Foils-header-wake-surfing-l.jpg, large], [/images/hero/Stinger-Foils-header-wake-surfing-xl.jpg, xlarge]" /></figure>
        </li>
        <li class="orbit-slide">
          <figure class="orbit-figure"><img class="orbit-image" alt="" data-interchange="[/images/hero/Stinger-Foils-header-prone-foil-s.jpg, small], [/images/hero/Stinger-Foils-header-prone-foil-m.jpg, medium], [/images/hero/Stinger-Foils-header-prone-foil-l.jpg, large], [/images/hero/Stinger-Foils-header-prone-foil-xl.jpg, xlarge]" /></figure>
        </li>
<!--
        <li class="orbit-slide">
          <figure class="orbit-figure"><img class="orbit-image" alt="" data-interchange="[/images/hero/Stinger-Foils-header-wake-foiling-s.jpg, small], [/images/hero/Stinger-Foils-header-wake-foiling-m.jpg, medium], [/images/hero/Stinger-Foils-header-wake-foiling-l.jpg, large], [/images/hero/Stinger-Foils-header-wake-foiling-xl.jpg, xlarge]" /></figure>
        </li>
        <li class="orbit-slide">
          <figure class="orbit-figure"><img class="orbit-image" alt="" data-interchange="[/images/hero/Stinger-Foils-header-double-s.jpg, small], [/images/hero/Stinger-Foils-header-double-m.jpg, medium], [/images/hero/Stinger-Foils-header-double-l.jpg, large], [/images/hero/Stinger-Foils-header-double-xl.jpg, xlarge]" /></figure>
        </li>
        <li class="orbit-slide">
          <figure class="orbit-figure"><img class="orbit-image" alt="" data-interchange="[/images/hero/Stinger-Foils-header-wake-surfing-women-s.jpg, small], [/images/hero/Stinger-Foils-header-wake-surfing-women-m.jpg, medium], [/images/hero/Stinger-Foils-header-wake-surfing-women-l.jpg, large], [/images/hero/Stinger-Foils-header-wake-surfing-women-xl.jpg, xlarge]" /></figure>
        </li>
-->
        </ul>
    </div>
  </div>