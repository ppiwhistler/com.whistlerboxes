<div class="videowrapper" style="background-image: url('/videos/Stinger-Foils-pump-foil.jpg');">
  <div class="video" class="show-for-medium-up">
    <video autoplay muted loop id="PumpFoil">
      <source src="/video/Stinger-Foils-pump-foil.mp4" type="video/mp4" />
      <source src="/video/Stinger-Foils-pump-foil.WebM" type="video/WebM" />
    </video>  
  </div>
</div>