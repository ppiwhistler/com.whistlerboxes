<div id="slideshow">
	<div class="grid-container grid-x align-center">
		<div class="small-12 medium-10 cell">
			<div class="orbit" role="region" aria-label="Whistler Boxes Homepage Slideshow" data-orbit>
				<div class="orbit-wrapper">
					<div class="orbit-controls">
						<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
						<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
					</div>
					<ul class="orbit-container">
						<li class="is-active orbit-slide">
							<figure class="orbit-figure">

								<img class="orbit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/slideshow/slide1.jpg" alt="slide 1">
							</figure>
						</li>
						<li class="orbit-slide">
							<figure class="orbit-figure">
								<img class="orbit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/slideshow/slide2.jpg" alt="slide 2">
							</figure>
						</li>
						<li class="orbit-slide">
							<figure class="orbit-figure">
								<img class="orbit-image" src="<?php echo get_template_directory_uri(); ?>/assets/img/slideshow/slide3.jpg" alt="slide 3">
							</figure>
						</li>
					</ul>
				</div>
		</div>
	</div>
</div>
</div>
