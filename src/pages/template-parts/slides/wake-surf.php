<div class="videowrapper" style="background-image: url('/videos/Stinger-wake-surf-drone.jpg');">
  <div class="show-for-medium-up">
    <video id="wakeShaper" autoplay="autoplay" loop="loop" muted="" type="video/mp4"> 
      <source src="/video/Stinger-wake-surf-drone.mp4" type="video/mp4" />
      <source src="/video/Stinger-wake-surf-drone.WebM" type="video/WebM" />
    </video>
  </div>
</div>