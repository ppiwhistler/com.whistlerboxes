<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<div class="grid-x grid-padding-x align-center">
			<div class="small-12 medium-10 cell">
				<h3 class="text-center" data-open="<?php the_title(); ?>"><?php the_title(); ?></h3>
			</div>
			<div class="doublespacer">&nbsp;</div>
		</div>
	</header>
	<div class="grid-x grid-padding-x align-center">
		<div class="small-12 medium-10 cell">
	    <?php the_content(); ?>
		</div>
	</div>
	<div class="reveal" id="<?php the_title(); ?>" data-reveal>
		<?php the_content(); ?>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
</article>
