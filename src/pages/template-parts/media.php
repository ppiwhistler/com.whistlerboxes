<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<div class="grid-x align-center-middle grid-padding-x">
  	  <div class="small-12 medium-5 medium-offset-1 cell small-order-2 medium-order-1">
      	<h2><?php the_title(); ?></h2>    	
      	<?php the_content(); ?>
  	  </div>	
      <div class="small-12 medium-4 medium-offset-1 cell small-order-1 medium-order-2">
        <?php the_post_thumbnail( 'category-thumb' ); ?>
      </div>	
		</div>
	</header>
</article>