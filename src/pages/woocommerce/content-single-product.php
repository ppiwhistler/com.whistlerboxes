<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<section class="crumbs">
  <div class="grid-x grid-padding-x">
    <div class="small-12 large-4 large-offset-1 cell">
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
      ?>
    </div>
  </div>
</section>
<section id="content">
  <article itemscope="" itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <section id="description" class="grid-container">
      <div class="grid-x align-center grid-padding-x">
        <div class="small-12 medium-6 large-8 cell">
          <div class="grid-x">
						<div class="small-12 cell">
							<img data-interchange="[<?php the_post_thumbnail_url('small' ); ?>, small], [<?php the_post_thumbnail_url( 'small' ); ?>, medium]">
						</div>
            <div class="small-12 cell">
              <h1 class="text-center" itemprop="name"><?php echo the_title(); ?></h1>
              <span itemprop="description"><?php the_content(); ?></span>
            </div>
            <hr>
            <div class="small-12 cell">
                <?php
            		/**
            		 * Hook: woocommerce_single_product_summary.
            		 *
            		 * @hooked woocommerce_template_single_title - 5
            		 * @hooked woocommerce_template_single_rating - 10
            		 * @hooked woocommerce_template_single_price - 10
            		 * @hooked woocommerce_show_product_sale_flash - 10
            		 * @hooked woocommerce_template_single_excerpt - 20
            		 * @hooked woocommerce_template_single_add_to_cart - 30
            		 * @hooked woocommerce_template_single_meta - 40
            		 * @hooked woocommerce_template_single_sharing - 50
            		 * @hooked WC_Structured_Data::generate_product_data() - 60
            		 */
            		do_action( 'woocommerce_single_product_summary' );
            		?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </article>
</section>
