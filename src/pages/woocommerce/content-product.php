<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$attachment_ids = $product->get_gallery_image_ids();
?>
<div class="cell">
	<div <?php wc_product_class(" "); ?> itemscope itemtype="http://schema.org/Product">
		<div class="grid-x align-center">
			<div class="small-12 medium-7 cell small-order-1 medium-order-1">
				<a href="<?php echo get_permalink($product_id); ?>"><?php echo the_post_thumbnail() ?></a>
			</div>
			<div class="small-12 cell small-order-2 medium-order-2">
				<a href="<?php echo get_permalink($product_id); ?>">
					<h2 itemprop="name" class="text-center"><?php echo the_title(); ?></h2>
				</a>
				<meta itemprop="brand" content="Whistler Boxes">
				<meta itemprop="sku" content="<?php echo the_title(); ?>">
				<meta itemprop="description" content="<?php the_excerpt(); ?>">
				<meta itemprop="image" src="https:<?php the_post_thumbnail_url(' '); ?>">
				<p class="text-center"><?php echo the_excerpt(); ?></p>
			</div>
			<div class="small-6 medium-4 cell small-order-3 medium-order-3">
				<a href="<?php echo get_permalink($product_id); ?>" class="button">Rent Now</a>
			</div>
		</div>
	</div>
</div>
