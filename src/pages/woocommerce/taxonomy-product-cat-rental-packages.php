<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
 do_action( 'woocommerce_before_main_content' );
 ?>

<?php include("/template-parts/slides/homepage.php"); ?>


<section class="crumbs grid-container">
  <div class="grid-x">
    <div class="small-12 medium-4 cell">
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
      ?>
    </div>
  </div>
</section>

<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
// 	do_action( 'woocommerce_archive_description' );

// starts pulling down products
if ( woocommerce_product_loop() ) {
	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked wc_print_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	// Establishing the current category ID
	//$current_cat = get_queried_object()->term_id;

	?>
	<section id="content" class="grid-container">
  	<div class="grid-x align-center grid-padding-x">
      <div class="small-12 medium-4 cell">
        <h2 class="text-center">Rental Packages</h2>
      </div>
    </div>
    <div class="grid-x align-center grid-padding-x grid-padding-y small-up-1 medium-up-3 large-up-4">
	    <?php
	  	  woocommerce_product_loop_start();

	    	if ( wc_get_loop_prop( 'total' ) ) {
	    		while ( have_posts() ) {
	    			the_post();

	    			/**
	    			 * Hook: woocommerce_shop_loop.
	    			 */
	    			do_action( 'woocommerce_shop_loop' );
	    			  wc_get_template_part( 'content', 'product' );
	    		}
	    	}
	      woocommerce_product_loop_end();
	  	 ?>
	  </div>
	</section>
	<?php

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
get_footer( 'shop' );
