<?php
/**
 * Author: Chris Armstrong - Patent Pending Ideas
 * URL: http://patentpendingideas.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );
/** subCategories for Journal Pages */
require_once( 'library/class-foundationpress-subCategory-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Gutenberg editor support */
require_once( 'library/gutenberg.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );

/** Breadcrumbs */
require_once( 'library/breadcrumbs.php' );

function switch_to_relative_url($html, $id, $caption, $title, $align, $url, $size, $alt)
{
$imageurl = wp_get_attachment_image_src($id, $size);
$relativeurl = wp_make_link_relative($imageurl[0]);
$html = str_replace($imageurl[0],$relativeurl,$html);

return $html;
}
add_filter('image_send_to_editor','switch_to_relative_url',10,8);

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

/* Aerluna Woocommerce customisation */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);  this was hiding the price on variable product, not composite.
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// don't show variable price range under title
add_filter( 'woocommerce_variable_sale_price_html', 'hide_variable_prices', PHP_INT_MAX, 2 );
add_filter( 'woocommerce_variable_price_html', 'hide_variable_prices', PHP_INT_MAX, 2 );
function hide_variable_prices() {return '';}

add_filter('woocommerce_show_variation_price', function() {return true;});


add_filter( 'body_class', 'wc_cat_names' );
function wc_cat_names( $classes ) {
		if(is_product() || is_product_category()){
				global $post;
				$terms = get_the_terms( $post->ID, 'product_cat' );
				foreach ($terms as $term) {
						$classes[] = $term->slug;
				}
		}
		return $classes;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
		if (in_array('current-menu-item', $classes) ){
				$classes[] = 'active ';
		}
		return $classes;
}

add_filter( 'woocommerce_variable_sale_price_html',
'lw_variable_product_price', 10, 2 );

add_filter( 'woocommerce_variable_price_html',
'lw_variable_product_price', 10, 2 );

function lw_variable_product_price( $v_price, $v_product ) {

// Regular Price
$v_prices = array( $v_product->get_variation_price( 'min', true ),
		$v_product->get_variation_price( 'max', true ) );

$v_price = $v_prices[0]!==$v_prices[1] ? sprintf(__('From: %1$s', 'woocommerce'),
		wc_price( $v_prices[0] ) ) : wc_price( $v_prices[0] );

// Sale Price
$v_prices = array( $v_product->get_variation_regular_price( 'min', true ),
		$v_product->get_variation_regular_price( 'max', true ) );
sort( $v_prices );
$v_saleprice = $v_prices[0]!==$v_prices[1] ? sprintf(__('From: %1$s','woocommerce')
				, wc_price( $v_prices[0] ) ) : wc_price( $v_prices[0] );

if ( $v_price !== $v_saleprice ) {
$v_price = '<del>'.$v_saleprice.$v_product->get_price_suffix() . '</del> <ins>' .
											 $v_price . $v_product->get_price_suffix() . '</ins>';
}
return $v_price;
}

//Hide "From:$X"
add_filter('woocommerce_get_price_html', 'lw_hide_variation_price', 10, 2);
function lw_hide_variation_price( $v_price, $v_product ) {
			$v_product_types = array( 'variable' );
	if ( in_array ( $v_product->product_type, $v_product_types ) ) {
			 return '';
	}
// return regular price
	return $v_price;

	}

add_action( 'woocommerce_email_after_order_table', 'custom_email_after_order_table', 10, 4 );
function custom_email_after_order_table( $order, $sent_to_admin, $plain_text, $email ) {

		// Displaying the shipping method used
		echo '<p><h4>'.__('Shipping', 'woocommerce').':</h4> '.$order->get_shipping_method().'</p>';

		$product_names = array();

		// Loop through order items
		foreach( $order->get_items() as $item_id => $item ){
				$product = $item->get_product(); // Get an instance of the WC_Product object
				$product_id = $item->get_product_id(); // Get the product ID

				// Set each product name in an array
				$product_names[] = $item->get_name(); // Get the product NAME
		}
		// Displaying the product names
		echo '<p><strong>'.__('Product names', 'woocommerce').':</strong> <br>'.implode( ', ', $product_names ).'</p>';
}

/* Sub Category Templates */
function new_subcategory_hierarchy() {
		$category = get_queried_object();

		$parent_id = $category->category_parent;

		$templates = array();

		if ( $parent_id == 0 ) {
				// Use default values from get_category_template()
				$templates[] = "category-{$category->slug}.php";
				$templates[] = "category-{$category->term_id}.php";
				$templates[] = 'category.php';
		} else {
				// Create replacement $templates array
				$parent = get_category( $parent_id );

				// Current first
				$templates[] = "category-{$category->slug}.php";
				$templates[] = "category-{$category->term_id}.php";

				// Parent second
				$templates[] = "category-{$parent->slug}.php";
				$templates[] = "category-{$parent->term_id}.php";
				$templates[] = 'category.php';
		}
		return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );
/* end of Subcategory templates */

/* Checks if a category is a descendant of another category */

if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
		function post_is_in_descendant_category( $cats, $_post = null ) {
				foreach ( (array) $cats as $cat ) {
						// get_term_children() accepts integer ID only
						$descendants = get_term_children( (int) $cat, 'category' );
						if ( $descendants && in_category( $descendants, $_post ) )
								return true;
				}
				return false;
		}
}

/* remove silly paragraph tags on everything.  it's wrecking my life */
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

/* Cart item Visibility */
add_filter( 'woocommerce_order_item_visible', 'wc_cp_order_item_visible', 10, 2 );
add_filter( 'woocommerce_widget_cart_item_visible', 'wc_cp_cart_item_visible', 10, 3 );
//add_filter( 'woocommerce_cart_item_visible', 'wc_cp_cart_item_visible' , 10, 3 );  /* hide items from cart */
add_filter( 'woocommerce_checkout_cart_item_visible', 'wc_cp_cart_item_visible', 10, 3 );
/** Visibility of components in orders.
 *
 * @param  boolean $visible
 * @param  array   $order_item
 * @return boolean
 */
function wc_cp_order_item_visible( $visible, $order_item ) {
		if ( ! empty( $order_item[ 'composite_parent' ] ) ) {
				$visible = false;
		}
		return $visible;
}
/**
 * Visibility of components in cart.
 *
 * @param  boolean $visible
 * @param  array   $cart_item
 * @param  string  $cart_item_key
 * @return boolean
 */
function wc_cp_cart_item_visible( $visible, $cart_item, $cart_item_key ) {
		if ( ! empty( $cart_item[ 'composite_parent' ] ) ) {
				$visible = false;
		}
		return $visible;
}

function alchemy_change_cart_table_price_display( $price, $values, $cart_item_key ) {
	 $slashed_price = $values['data']->get_price_html();
	 $is_on_sale = $values['data']->is_on_sale();
	 if ( $is_on_sale ) {
			$price = $slashed_price;
	 }
	 return $price;
}

/**
 * Hide the "In stock" message on product page.
 *
 * @param string $html
 * @param string $text
 * @param WC_Product $product
 * @return string
 */
function my_wc_hide_in_stock_message( $html, $text, $product ) {
	$availability = $product->get_availability();

	if ( isset( $availability['class'] ) && 'in-stock' === $availability['class'] ) {
		return '';
	}

	return $html;
}

add_filter( 'woocommerce_stock_html', 'my_wc_hide_in_stock_message', 10, 3 );

/**
 * Trim zeros in price decimals
 **/
add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

add_action('woocommerce_thankyou', function ($order_id) {
	$order        = new \WC_Order( $order_id );
	$variables = [
		'order_id'     => $order_id,
		'num_items'    => count($order->get_items()),
		'value'        => floatval($order->get_total()),
		'currency'     => get_woocommerce_currency(),
	];
	$variables = json_encode($variables);
	echo "<script>\ndataLayer.push($variables);</script>";
}, 20);


//* Enqueue scripts and styles
add_action( 'wp_enqueue_scripts', 'crunchify_disable_woocommerce_loading_css_js' );

function crunchify_disable_woocommerce_loading_css_js() {

	// Check if WooCommerce plugin is active
	if( function_exists( 'is_woocommerce' ) ){

		// Check if it's any of WooCommerce page
		if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) {

			# Styles
				wp_dequeue_style( 'woocommerce-general' );
				wp_dequeue_style( 'woocommerce-layout' );
				wp_dequeue_style( 'woocommerce-smallscreen' );
				wp_dequeue_style( 'woocommerce_frontend_styles' );
				wp_dequeue_style( 'woocommerce_fancybox_styles' );
				wp_dequeue_style( 'woocommerce_chosen_styles' );
				wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
				# Scripts
				wp_dequeue_script( 'wc_price_slider' );
				wp_dequeue_script( 'wc-single-product' );
				wp_dequeue_script( 'wc-add-to-cart' );
				wp_dequeue_script( 'wc-cart-fragments' );
				wp_dequeue_script( 'wc-checkout' );
				wp_dequeue_script( 'wc-add-to-cart-variation' );
				wp_dequeue_script( 'wc-single-product' );
				wp_dequeue_script( 'wc-cart' );
				wp_dequeue_script( 'wc-chosen' );
				wp_dequeue_script( 'woocommerce' );
				wp_dequeue_script( 'prettyPhoto' );
				wp_dequeue_script( 'prettyPhoto-init' );
				wp_dequeue_script( 'jquery-blockui' );
				wp_dequeue_script( 'jquery-placeholder' );
				wp_dequeue_script( 'fancybox' );
				wp_dequeue_script( 'jqueryui' );
		}
	}
}

?>
