<div id="social" class="grid-x grid-padding-x medium-align-center align-right align-middle small-up-6 small-offset-6 medium-offset-7 doublespacer">
	<div class="cell"><a href="https://www.facebook.com/stingerfoils"><i class="fa fa-facebook fa-1x" aria-hidden="true">&nbsp;</i></a></div>
	<div class="cell"><a href="https://www.instagram.com/stingerfoils/"><i class="fa fa-instagram fa-1x" aria-hidden="true">&nbsp;</i></a></div>
</div>
