<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>
	<section id="footer">
		<div class="grid-x align-center align-middle copyright">
			<div class="small-12 medium-4 cell">
				<?php foundationpress_top_bar_footer(); ?>
			</div>
			<div class="small-12 medium-2 medium-offset-4 cell">
				<div class="grid-x small-up-1">
					<p class="text-center">&copy; <?php echo date("Y"); ?> Whistlerboxes.com All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</section>

<?php wp_footer(); ?>

<!-- JS Plugins -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/app.js"></script>
<noscript>JavaScript is unavailable or disabled; so you are probably going to miss out on a few things. Everything should still work, but not nearly as cool is if it was.</noscript></body>
</html>
