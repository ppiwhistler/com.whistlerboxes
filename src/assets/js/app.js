import $ from 'jquery';
import whatInput from 'what-input';
import mixitup from 'mixitup';
import mixitupMultifilter from 'mixitup-Multifilter';
import ScrollOut from "scroll-out";

window.$ = $;

import Foundation from 'foundation-sites';

ScrollOut({
  threshhold: 0.5,
   cssProps: {
      visibleY: true
   }
});

jQuery('p:empty').remove();

$(document).foundation();

mixitup.use(mixitupMultifilter);
var containerEl = document.querySelector('.container');
  var mixer = mixitup(containerEl, {
    multifilter: {
      enable: true
    },
    animation: {
        effects: "fade scale(0.75) translateZ(-100px)",
        reverseOut: true,
        nudge: false // Disable nudging to create a carousel-like effect
    }
 });
